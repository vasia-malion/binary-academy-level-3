const {UserRepository} = require("../repositories/userRepository");
const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
    let error = 0;
    const userData = req.body;

    for (let field in user) {
        if (field === "id") {
            continue;
        }
        if (!userData.hasOwnProperty(field)){
            error++
        }
    }

    if (error) {
        return res.status(400).json({error: true, message: 'Data is invalid!'});
    }

    const findEmail = UserRepository.getOne({email: userData.email});
    const findPhone = UserRepository.getOne({phoneNumber: userData.phoneNumber});

    if (findEmail) {
        return res.status(400).send({error: true, message: 'This email has already used!'});
    }

    if (findPhone) {
        return res.status(400).send({error: true, message: 'This phone has already used!'});
    }

    if (!validateEmail(userData.email, '@gmail.com')) {
        return res.status(400).send({error: true, message: 'This email is not correct. Must be ***@gmail.com!'});
    }

    if (!validatePhone(userData.phoneNumber, '+380')) {
        return res.status(400).send({error: true, message: 'Phone number is invalid!'});
    }

    if (userData.password.length <= 3) {
        return res.status(400).send({error: true, message: 'Password is too short!'});
    }

    next();
}

const updateUserValid = (req, res, next) => {
    next();
}

const validateEmail = (email, type) => {
    return email.endsWith(type)
}

const validatePhone = (phone, type) => {
    if (phone.startsWith(type)) {
        if (phone.length === 13) {
            return true;
        }
    }
    return false;
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
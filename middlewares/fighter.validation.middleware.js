const {FighterRepository} = require("../repositories/fighterRepository");
const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    let error = 0;
    const fighterData = req.body;

    fighterData.health = fighterData.health ? fighterData.health : 100;

    for (let field in fighter) {
        if (field === "id" || field === "health") {
            continue;
        }
        if (!fighterData.hasOwnProperty(field)){
            error++
        }
    }

    if (error) {
        return res.status(400).json({error: true, message: 'Data is invalid!'});
    }

    const findName = FighterRepository.getOne({name: fighterData.name});

    if (findName) {
        return res.status(400).send({error: true, message: 'This name has already used!'});
    }

    const powerValidation = !Number.isInteger(fighterData.power) || fighterData.power <= 1 || fighterData.power >= 100;
    const defenseValidation = !Number.isInteger(fighterData.defense) || fighterData.defense <= 1 || fighterData.defense >= 10;
    const healthValidation = !Number.isInteger(fighterData.health) || fighterData.health <= 80 || fighterData.health >= 120;

    if (powerValidation) {
        return res.status(400).json({error: true, message: 'Power is invalid!'});
    }

    if (defenseValidation) {
        return res.status(400).json({error: true, message: 'Defense is invalid!'});
    }

    if (healthValidation) {
        return res.status(400).json({error: true, message: 'Health is invalid!'});
    }

    next();
}

const updateFighterValid = (req, res, next) => {
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        const {email, password} = req.body;
        const user = AuthService.login({email});

        if (user && password === user.password) {
            return res.json(user);
        } else {
            return {
                error: true,
                message: 'Data is invalid'
            }
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;
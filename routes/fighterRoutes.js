const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res) => {
    const fighters = FighterService.getAllFighters();

    if (fighters) {
        res.json(fighters)
    } else {
        return res.status(404).json({
            error: true,
            message: 'Fighters not found'
        })
    }
})

router.get('/:id', (req, res) => {
    const fighter = FighterService.search(req.body.id);

    if (fighter) {
        return res.json(fighter);
    } else {
        return res.status(404).json({
            error: true,
            message: 'Fighter not found!'
        })
    }
})

router.post('/', createFighterValid, (req, res) => {
    const fighter = FighterService.createFighter(req.body);

    if (fighter) {
        res.json(fighter)
    } else {
        return res.status(400).json({
            error: true,
            message: 'Data is invalid'
        })
    }
})

router.put('/:id', updateFighterValid, (req,res) => {
    const fighter = FighterService.updateFighter(req.body);

    if (fighter) {
        return res.json(fighter);
    } else {
        return res.status(400).json({
            error: true,
            message: 'Fighter not found'
        })
    }
});

router.delete('/:id', (req,res) => {
    const fighter = FighterService.deleteFighter(req.body.id);

    if (fighter) {
        return res.json(fighter);
    } else {
        return res.status(400).json({
            error: true,
            message: 'Fighter not found'
        })
    }
});

module.exports = router;
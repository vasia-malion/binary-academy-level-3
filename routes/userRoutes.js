const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res) => {
    const users = UserService.getAllUsers();

    if (users) {
        return res.json(users);
    } else {
        return res.status(404).json({
            error: true,
            message: 'Users not found!'
        })
    }
})

router.get('/:id', (req, res) => {
    const user = UserService.search(req.body.id);

    if (user) {
        return res.json(user);
    } else {
        return res.status(404).json({
            error: true,
            message: 'User not found!'
        })
    }
})

router.post('/', createUserValid, (req,res, next)=>{
    const data = req.body;
    const user = UserService.createUser(data);

    if (user) {
        return res.json(user);
    } else {
        return res.status(400).json({
            error: true,
            message: 'Data is invalid'
        })
    }
});

router.put('/:id', updateUserValid, (req,res) => {
    const user = UserService.updateUser(req.body);

    if (user) {
        return res.json(user);
    } else {
        return res.status(400).json({
            error: true,
            message: 'User not found'
        })
    }
});

router.delete('/:id', (req,res) => {
    const user = UserService.deleteUser(req.body.id);

    if (user) {
        return res.json(user);
    } else {
        return res.status(404).json({
            error: true,
            message: 'User not found'
        })
    }
});

module.exports = router;
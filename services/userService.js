const { UserRepository } = require('../repositories/userRepository');

class UserService {

    deleteUser(id){
        const deletedUser = UserRepository.delete(id);
        if(!deletedUser){
            return null;
        }
        return deletedUser;
    }

    updateUser(id, dataYoUpdate) {
        const updatedUser = UserRepository.update(id, dataYoUpdate);
        if(!updatedUser){
            return null;
        }
        return updatedUser;
    }

    createUser(data){
        const newUser = UserRepository.create(data);
        if(!newUser){
            return null;
        }
        return newUser
    }

    getAllUsers() {
        const users = UserRepository.getAll();
        if(!users) {
            return null;
        }
        return users;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();
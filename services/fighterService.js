const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    deleteFighter (id){
        const deletedUser = FighterRepository.delete(id);
        if(!deletedUser){
            return null;
        }
        return deletedUser;
    }

    updateFighter(id, dataYoUpdate) {
        const updatedUser = FighterRepository.update(id, dataYoUpdate);
        if(!updatedUser){
            return null;
        }
        return updatedUser;
    }

    createFighter(data){
        const newUser = FighterRepository.create(data);
        if(!newUser){
            return null;
        }
        return newUser
    }

    getAllFighters() {
        const users = FighterRepository.getAll();
        if(!users) {
            return null;
        }
        return users;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();